<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>"admin",
            'email'=>"admin@admin.com",
            'password'=>\Hash::make("admin@123456"),
            'user_type'=>"admin",
        ]);
        for($i = 0;$i < 100;$i++) {
            User::create([
                'name'=>"client-".$i,
                'email'=>"client".$i."@task.com",
                'password'=>\Hash::make("admin@123456"),
                'user_type'=>"client",
            ]);
        }
    }
}
