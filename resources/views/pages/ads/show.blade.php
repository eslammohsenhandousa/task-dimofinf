@extends('layouts.master')
@section('PageTitle',$breadcrumb['title'])
@section('PageContent')
@includeIf('layouts.inc.breadcrumb')


<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body" style=" position: relative; ">
                <h5 class="font-size-15">@lang('Ads Details') :</h5>
                <p class="text-muted">
                    {!! $ad->description !!}
                </p>
            </div>
        </div>
    </div>

    <div class="col-lg-4">

        <div class="">
            <div class="table-responsive">
                <table class="table project-list-table table-nowrap align-middle table-borderless">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">##</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                @lang('Title')
                            </td>
                            <td>
                                {{ $ad->title }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                @lang('Name')
                            </td>
                            <td>
                                {{ $ad->user->name }} ( {{ ucfirst($ad->user->user_type ?? '') }} )
                            </td>
                        </tr>
                        <tr>
                            <td>
                                @lang('Email')
                            </td>
                            <td>
                                <a href="mailto:{{ $ad->user->email }}">{{ $ad->user->email }}</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                @lang('Created At')
                            </td>
                            <td>
                                {{ $ad->created_at }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">@lang('Ads Image')</h4>
                <div class="table-responsive">
                    <center>
                        <img src="{{ display_image_by_model($ad,'image') }}" style=" width: 100%; height: 100px; ">
                    </center>
                </div>
            </div>
        </div>


    </div>
</div>


@endsection

