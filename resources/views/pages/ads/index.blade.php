@extends('layouts.master')
@section('PageTitle',$breadcrumb['title'])
@section('PageContent')
@includeIf('layouts.inc.breadcrumb')

<div style=" margin-bottom: 14px; position: relative; text-align: right; ">
    <a type="button" class="btn btn-primary my-action"  href="{{route('ads.create')}}">@lang('Create new ads')</a>
</div>


@if ($lists->count() > 0)

    <div class="row">
        <div class="col-lg-12">
            <div class="">
                <div class="table-responsive">
                    <table class="table project-list-table table-nowrap align-middle table-borderless">
                        <thead>
                            <tr>
                                <th scope="col">@lang('User')</th>
                                <th scope="col">@lang('Title')</th>
                                <th scope="col">@lang('Created At')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($lists as $list)
                                <tr>
                                    <td>
                                        {{ $list->user->name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $list->title ?? '' }}
                                    </td>
                                    <td>
                                        {{ $list->created_at}}
                                    </td>
                                    <td style="display: inline-flex;">
                                        <a style="margin-right: 5px;"
                                            class="btn btn-outline-secondary btn-sm edit"
                                            href="{{ route('ads.show',$list->id) }}">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        @if (\Auth::user()->id == $list->user_id)
                                            <a style="margin-right: 5px;"
                                                class="btn btn-outline-secondary btn-sm edit"
                                                href="{{ route('ads.edit',$list->id) }}">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                            {!! action_table_delete(route('ads.destroy',$list->id),$list->id) !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{ $lists->links('layouts.inc.paginator') }}
@else

    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <div class="row justify-content-center mt-5">
                    <div class="col-sm-4">
                        <div class="maintenance-img">
                            <img src="{{ url('assets/images/verification-img.png') }}" alt="" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <h4 class="mt-5">@lang("Let's get started")</h4>
                <p class="text-muted">@lang("Oops, We don't have users").</p>
            </div>
        </div>
    </div>


@endif

@endsection
