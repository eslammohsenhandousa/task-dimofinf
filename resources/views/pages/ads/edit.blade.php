@extends('layouts.master')
@section('PageTitle',$breadcrumb['title'])
@section('PageContent')
@includeIf('layouts.inc.breadcrumb')



<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('ads.update',$ad->id) }}" method="post" enctype="multipart/form-data">
                    <h5 class="font-size-14">
                        <i class="mdi mdi-arrow-right text-primary"></i>
                        @lang('General information')
                    </h5>

                    <div class="mb-3 row">
                        <label for="user_avatar" class="col-form-label col-lg-2">@lang('Title')</label>
                        <div class="col-lg-10">
                            <input class="form-control" placeholder="@lang('Title')" value="{{ old('title',$ad->title) }}" type="text" name="title" id="user_avatar">
                            @error('name')
                                <span style="color:red;">
                                    {{ $errors->first('name') }}
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="mb-3 row">
                        <label for="user_avatar" class="col-form-label col-lg-2">@lang('Description')</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" placeholder="@lang('Description')" name="description" cols="30" rows="10">{{ old('description',$ad->description) }}</textarea>
                            @error('description')
                                <span style="color:red;">
                                    {{ $errors->first('description') }}
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="mb-3 row">
                        <label for="user_avatar" class="col-form-label col-lg-2">@lang('Image')</label>
                        <div class="col-lg-9">
                            <input class="form-control" type="file" name="image" id="user_avatar">
                            @error('image')
                                <span style="color:red;">
                                    {{ $errors->first('image') }}
                                </span>
                            @enderror
                        </div>
                        @if(isset($ad))
                            <div class="col-lg-1">
                                <a href="{{ display_image_by_model($ad,'image') }}" target="_blank">
                                    <img src="{{ display_image_by_model($ad,'image') }}" alt="" class="rounded-circle header-profile-user">
                                </a>
                            </div>
                        @endif
                    </div>

                    <div class="row" style=" margin-top: 20px; ">
                        <div style="text-align: right">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-primary w-md">@lang('Submit')</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div> <!-- end col -->
</div>


@endsection
