<script>
    $('.choose_project_chart').change(function(){
        let projectId = $(this).val();
        if(projectId == 0) {
            toastr["warning"]("{{ __('Please choose project') }}")
        } else {
            $.ajax({
                url: "{{route('home.projects-chart')}}",
                type: "post",
                data:{project_id:projectId},
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#bar_chart').hide();
                    $('#bar_chart_'+projectId).show();
                    var visibleId = $('.apex-charts:visible').prop('id');
                    options={chart:{height:350,type:"bar",toolbar:{show:!1}},
                        plotOptions:{bar:{horizontal:!0}},
                        dataLabels:{enabled:!1},
                        series:[{data:data[1]}],
                        colors:["#34c38f"],
                        grid:{borderColor:"#f1f1f1"},
                        xaxis:{categories:data[0]}};
                    (chart=new ApexCharts(document.querySelector("bar_chart_"+projectId),options)).render();
                }
            });
        }
    });
</script>
