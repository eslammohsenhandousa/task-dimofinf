<script>
    $('.choose_project').change(function(){
        $( ".staffs" ).html( '' );
        let projectId = $(this).val();
        let userId = $('.staffs').val();
        let year = $('.year').val();
        if(projectId == 0) {
            toastr["warning"]("{{ __('Please choose project') }}")
        } else {
            $.get("{{ url('/reports/ajax-users') }}/"+projectId, function( data ) {
                $( ".staffs" ).html( data );
            });
            $.ajax({
                url: "{{route('home.tasks-chart')}}",
                type: "post",
                data:{project_id:projectId , user_id : userId , year : year},
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {

                    Highcharts.chart('area-chart', {
                        title: {
                            text: 'Tasks For Projects and Users'
                        },
                        subtitle: {
                            text: 'Asga Management'
                        },
                        xAxis: {
                            categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                                'October', 'November', 'December'
                            ]
                        },
                        yAxis: {
                            title: {
                                text: 'Number of Tasks'
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },
                        plotOptions: {
                            series: {
                                allowPointSelect: true
                            }
                        },
                        series: [{
                            name: 'New',
                            data: data[0]
                        },
                            {
                                name: 'Hold',
                                data: data[1]
                            },
                            {
                                name: 'In-Progress',
                                data: data[2]
                            },
                            {
                                name: 'Completed',
                                data: data[3]
                            }],
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    });
                }
            });
        }
    });

    $('.staffs').change(function(){
        let projectId = $('.choose_project').val();
        let userId = $(this).val();
        let year = $('.year').val();
        if(userId == 0) {
            toastr["warning"]("{{ __('Please choose user') }}")
        } else {
            $.ajax({
                url: "{{route('home.tasks-chart')}}",
                type: "post",
                data:{project_id:projectId , user_id : userId , year:year},
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {

                    Highcharts.chart('area-chart', {
                        title: {
                            text: 'Tasks For Projects and Users'
                        },
                        subtitle: {
                            text: 'Asga Management'
                        },
                        xAxis: {
                            categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                                'October', 'November', 'December'
                            ]
                        },
                        yAxis: {
                            title: {
                                text: 'Number of Tasks'
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },
                        plotOptions: {
                            series: {
                                allowPointSelect: true
                            }
                        },
                        series: [{
                            name: 'New',
                            data: data[0]
                        },
                            {
                                name: 'Hold',
                                data: data[1]
                            },
                            {
                                name: 'In-Progress',
                                data: data[2]
                            },
                            {
                                name: 'Completed',
                                data: data[3]
                            }],
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    });

                }
            });
        }
    });

    $('.year').change(function(){
        let projectId = $('.choose_project').val();
        let userId = $('.staffs').val();
        let year = $(this).val();
        if(projectId == 0 && userId == 0) {
            toastr["warning"]("{{ __('Please choose project or user at least') }}")
        } else {
            $.ajax({
                url: "{{route('home.tasks-chart')}}",
                type: "post",
                data:{project_id:projectId , user_id : userId , year:year},
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    Highcharts.chart('area-chart', {
                        title: {
                            text: 'Tasks For Projects and Users'
                        },
                        subtitle: {
                            text: 'Asga Management'
                        },
                        xAxis: {
                            categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                                'October', 'November', 'December'
                            ]
                        },
                        yAxis: {
                            title: {
                                text: 'Number of Tasks'
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },
                        plotOptions: {
                            series: {
                                allowPointSelect: true
                            }
                        },
                        series: [{
                            name: 'New',
                            data: data[0]
                        },
                            {
                                name: 'Hold',
                                data: data[1]
                            },
                            {
                                name: 'In-Progress',
                                data: data[2]
                            },
                            {
                                name: 'Completed',
                                data: data[3]
                            }],
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    });
                }
            });
        }
    });
</script>
