@extends('layouts.master')
@section('PageTitle',$breadcrumb['title'])
@section('PageContent')
@includeIf('layouts.inc.breadcrumb')

<div style=" margin-bottom: 14px; position: relative; text-align: right; ">
    <a type="button" class="btn btn-primary my-action"  href="{{route('users.create')}}">@lang('Create new user')</a>
</div>


@if ($lists->count() > 0)

    <div class="row">
        <div class="col-lg-12">
            <div class="">
                <div class="table-responsive">
                    <table class="table project-list-table table-nowrap align-middle table-borderless">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Name')</th>
                                <th scope="col">@lang('Email')</th>
                                <th scope="col">@lang('Created At')</th>
                                <th scope="col">@lang('Roles')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($lists as $list)
                                <tr>
                                    <td>
                                        {{ $list->name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $list->email ?? '' }}
                                    </td>
                                    <td>
                                        {{ $list->created_at}}
                                    </td>
                                    <td>
                                        {{ ucfirst($list->user_type) }}
                                    </td>
                                    <td style="display: inline-flex;">
                                        <a style="margin-right: 5px;"
                                            class="btn btn-outline-secondary btn-sm edit"
                                            href="{{ route('users.edit',$list->id) }}">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        {!! action_table_delete(route('users.destroy',$list->id),$list->id) !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{ $lists->links('layouts.inc.paginator') }}
@else

    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <div class="row justify-content-center mt-5">
                    <div class="col-sm-4">
                        <div class="maintenance-img">
                            <img src="{{ url('assets/images/verification-img.png') }}" alt="" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <h4 class="mt-5">@lang("Let's get started")</h4>
                <p class="text-muted">@lang("Oops, We don't have users").</p>
            </div>
        </div>
    </div>


@endif

@endsection
