@extends('layouts.master')
@section('PageTitle',$breadcrumb['title'])
@section('PageContent')
@includeIf('layouts.inc.breadcrumb')



<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('users.update',$user->id) }}" method="post" enctype="multipart/form-data">
                    <h5 class="font-size-14">
                        <i class="mdi mdi-arrow-right text-primary"></i>
                        @lang('General information')
                    </h5>

                    <div class="mb-3 row">
                        <label for="user_avatar" class="col-form-label col-lg-2">@lang('Name')</label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{ old('name',$user->name) }}" type="text" name="name" id="user_avatar">
                            @error('name')
                                <span style="color:red;">
                                    {{ $errors->first('name') }}
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="mb-3 row">
                        <label for="user_avatar" class="col-form-label col-lg-2">@lang('Email')</label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{ old('email',$user->email) }}" type="email" name="email" required>
                            @error('email')
                            <span style="color:red;">
                                    {{ $errors->first('email') }}
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="mb-3 row">
                        <label for="user_avatar" class="col-form-label col-lg-2">@lang('Password')</label>
                        <div class="col-lg-10">
                            <input class="form-control" type="password" name="password">
                            @error('password')
                            <span style="color:red;">
                                    {{ $errors->first('password') }}
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="mb-3 row">
                        <label for="user_avatar" class="col-form-label col-lg-2">@lang('Image')</label>
                        <div class="col-lg-9">
                            <input class="form-control" type="file" name="avatar" id="user_avatar">
                            @error('avatar')
                                <span style="color:red;">
                                    {{ $errors->first('avatar') }}
                                </span>
                            @enderror
                        </div>
                        @if(isset($user))
                            <div class="col-lg-1">
                                <a href="{{ display_image_by_model($user,'avatar') }}" target="_blank">
                                    <img src="{{ display_image_by_model($user,'avatar') }}" alt="" class="rounded-circle header-profile-user">
                                </a>
                            </div>
                        @endif
                    </div>

                    <div class="row col-12 mb-3">

                        <label for="head" class="col-form-label col-lg-2">
                            @lang('User Type')
                        </label>
                        <div class="col-lg-10">
                            <select name="user_type" class="form-control">
                                <option @if($user->user_type == "admin") selected @endif value="admin">admin</option>
                                <option @if($user->user_type == "client") selected @endif value="client">client</option>
                            </select>
                            @error('user_type')
                                <span style="color:red;">
                                    {{ $errors->first('user_type') }}
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row" style=" margin-top: 20px; ">
                        <div style="text-align: right">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-primary w-md">@lang('Submit')</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div> <!-- end col -->
</div>


@endsection
