@extends('layouts.master')
@section('PageTile',$breadcrumb['title'])
@section('PageContent')
@includeIf('layouts.inc.breadcrumb')


<div class="checkout-tabs">
    <div class="row">
        <div class="col-xl-2 col-sm-3">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-information-tab"
                        data-bs-toggle="pill"
                        href="#v-pills-information"
                        role="tab"
                        aria-controls="v-pills-information"
                        aria-selected="true">
                        <i class= "bx bx-body d-block check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">{{ __('Information') }}</p>
                    </a>
                    <a class="nav-link" id="v-pills-password-tab"
                        data-bs-toggle="pill"
                        href="#v-pills-password"
                        role="tab"
                        aria-controls="v-pills-password"
                        aria-selected="true">
                        <i class= "bx bx-spa d-block check-nav-icon mt-4 mb-2"></i>
                        <p class="fw-bold mb-4">{{ __('Password') }}</p>
                    </a>
                    @can('custody.index')
                        <a class="nav-link" id="v-pills-custody-tab"
                            data-bs-toggle="pill"
                            href="#v-pills-custody"
                            role="tab"
                            aria-controls="v-pills-custody"
                            aria-selected="true">
                            <i class= "bx bx-windows d-block check-nav-icon mt-4 mb-2"></i>
                            <p class="fw-bold mb-4">{{ __('Custody') }}</p>
                        </a>
                    @endcan

            </div>
        </div>
        <div class="col-xl-10 col-sm-9">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-information" role="tabpanel" aria-labelledby="v-pills-information-tab">
                                <div>
                                    <form action="{{ route('profile.store') }}" method="post" enctype="multipart/form-data">

                                        <div class="mb-3 row">
                                            <label for="example-text-input" class="col-md-2 col-form-label">@lang('Name')</label>
                                            <div class="col-md-10">
                                                <input class="form-control" type="text" value="{{ old('name',$user->name) }}" name="name" placeholder="@lang('Name')"
                                                    id="example-text-input">
                                                    @error('name')
                                                        <span style="color:red;">
                                                            {{ $errors->first('name') }}
                                                        </span>
                                                    @enderror
                                            </div>
                                        </div>

                                        <div class="mb-3 row">
                                            <label for="example-text-input" class="col-md-2 col-form-label">@lang('Email')</label>
                                            <div class="col-md-10">
                                                <input class="form-control" name="email" value="{{ old('email',$user->email) }}" type="email" placeholder="@lang('Email')"
                                                    id="example-text-input">
                                                    @error('email')
                                                        <span style="color:red;">
                                                            {{ $errors->first('email') }}
                                                        </span>
                                                    @enderror
                                            </div>
                                        </div>

                                        <div class="mb-3 row">
                                            <label for="user_avatar" class="col-form-label col-lg-2">@lang('Image')</label>
                                            <div class="col-lg-9">
                                                <input class="form-control" type="file" name="avatar" id="user_avatar">
                                                @error('avatar')
                                                    <span style="color:red;">
                                                        {{ $errors->first('avatar') }}
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-1">
                                                <img src="{{ display_image_by_model($user,'avatar') }}" alt="" class="rounded-circle header-profile-user">
                                            </div>
                                        </div>

                                        <div class="row" style=" margin-top: 20px; ">
                                            @csrf
                                            <div style="text-align: right">
                                                <button type="submit" class="btn btn-primary w-md">@lang('Submit')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
                                <div>
                                    <form action="{{ route('profile.store') }}" method="post" enctype="multipart/form-data">
                                        <div class="mb-3 row">
                                            <label for="example-text-input" class="col-md-2 col-form-label">@lang('Current Password')</label>
                                            <div class="col-md-10 password-group">
                                                <input class="form-control password-box" name="current_password" type="password" placeholder="@lang('Current Password')" style="width: 95%;">
                                                <a class="btn btn-light password-visibility" type="button" style="position: absolute;top: 1px;right: 15px;padding: 6px;"><i class="mdi mdi-eye-outline"></i></a>
                                                @error('current_password')
                                                <span style="color:red;">
                                                            {{ $errors->first('current_password') }}
                                                        </span>
                                                @enderror

                                             </div>
                                        </div>

                                        <div class="mb-3 row ">
                                            <label for="example-text-input" class="col-md-2 col-form-label">@lang('New Password')</label>
                                            <div class="col-md-10 password-group">
                                                <input class="form-control password-box" name="new_password" type="password" placeholder="@lang('New Password')" style="width: 95%;">
                                                <a class="btn btn-light password-visibility" type="button" style="position: absolute;top: 1px;right: 15px;padding: 6px;"><i class="mdi mdi-eye-outline"></i></a>

                                                @error('new_password')
                                                <span style="color:red;">
                                                            {{ $errors->first('new_password') }}
                                                        </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="mb-3 row">
                                            <label for="example-text-input" class="col-md-2 col-form-label">@lang('Confirm Password')</label>
                                            <div class="col-md-10 password-group">
                                                <input class="form-control password-box" name="password_confirmation" type="password" placeholder="@lang('Confirm Password')" style="width: 95%;">
                                                <a class="btn btn-light password-visibility" type="button" style="position: absolute;top: 1px;right: 15px;padding: 6px;"><i class="mdi mdi-eye-outline"></i></a>

                                                @error('password_confirmation')
                                                <span style="color:red;">
                                                            {{ $errors->first('password_confirmation') }}
                                                        </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="row" style=" margin-top: 20px; ">
                                            @csrf
                                            <div style="text-align: right">
                                                <button type="submit" class="btn btn-primary w-md">@lang('Submit')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @can('custody.index')
                                <div class="tab-pane fade" id="v-pills-custody" role="tabpanel" aria-labelledby="v-pills-custody-tab">
                                    <div>
                                        @if(\Auth::user()->items()->count() > 0)
                                        <table class="table project-list-table table-nowrap align-middle table-borderless">
                                            <thead>
                                            <tr>
                                                <th scope="col" style="width: 100px">#</th>
                                                <th scope="col">@lang('Take On Date')</th>
                                                <th scope="col">@lang('Take Off Date')</th>
                                                <th scope="col">@lang('Action')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach (\Auth::user()->items()->get() as $item)
                                                <tr>
                                                    <td>
                                                        {{ $loop->index + 1 }}
                                                    </td>
                                                    <td>
                                                        {!! $item->pivot->take_on_date !!}
                                                    </td>
                                                    <td>
                                                        {!! $item->pivot->take_off_date ? $item->pivot->take_off_date : 'Unknown' !!}
                                                    </td>
                                                    <td style="display: inline-flex;">
                                                        @if(!\Auth::user()->can('custody.edit') && !\Auth::user()->can('custody.destroy'))
                                                            <span class="make_pad badge bg-warning" style="font-size: 12px">
                                                    @lang("Don't have permission to edit or delete")
                                                    </span>
                                                        @else
                                                            @can('custody.edit')
                                                                <a style="margin-right: 5px;" class="btn btn-outline-secondary btn-sm edit"
                                                                data-bs-toggle="offcanvas"
                                                                data-bs-target="#offcanvasWithBothOptions{{$item->pivot->id}}"
                                                                data-title="@lang('Edit custody')"
                                                                aria-controls="offcanvasWithBothOptions{{$item->pivot->id}}">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </a>
                                                            @endcan
                                                                @can('custody.destroy')
                                                                    {!! action_table_delete(route('custody.destroy',[$item->pivot->item_id,\Auth::user()->id]),$item->pivot->id) !!}
                                                                @endcan
                                                        @endif
                                                    </td>
                                                </tr>

                                                <div class="offcanvas offcanvas-start" data-bs-scroll="true" tabindex="-1" id="offcanvasWithBothOptions{{$item->pivot->id}}" aria-labelledby="offcanvasWithBothOptionsLabel">
                                                    <div class="offcanvas-header">
                                                        <h5 class="offcanvas-title" id="offcanvasWithBothOptionsLabel">
                                                            @lang('Edit custody')
                                                        </h5>
                                                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                                                    </div>
                                                    <div class="offcanvas-body">
                                                        <form action="{{ route('custody.update',[$item->pivot->item_id]) }}" method="post">
                                                            <div class="mb-3 row">
                                                                <div class="col-md-12">
                                                                    <input type="date" value="{{ $item->pivot->take_on_date }}" name="take_on_date" class="form-control">
                                                                    @error('take_on_date')
                                                                    <span style="color:red;">
                                                                        {{ $errors->first('take_on_date') }}
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="mb-3 row">
                                                                <div class="col-md-12">
                                                                    <input type="date" value="{{ $item->pivot->take_off_date }}" min="{{ $item->pivot->take_on_date }}" name="take_off_date" class="form-control">
                                                                    @error('take_off_date')
                                                                    <span style="color:red;">
                                                                        {{ $errors->first('take_off_date') }}
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="user_id" value="{{\Auth::user()->id}}">
                                                            <div class="row" style=" margin-top: 20px; ">
                                                                <div style="text-align: right">
                                                                    @csrf
                                                                    @method('PUT')
                                                                    <button type="submit" class="btn btn-primary w-md">@lang('Submit')</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                            @endforeach
                                            </tbody>
                                        </table>
                                        @else
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <div class="row justify-content-center mt-5">
                                                            <div class="col-sm-4">
                                                                <div class="maintenance-img">
                                                                    <img src="assets/images/a1.png" alt="" class="img-fluid mx-auto d-block">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4 class="mt-5">@lang("Let's get started")</h4>
                                                        <p class="text-muted">@lang("Oops, We don't have items").</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endcan
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@push('scripts')
<script>
    $(function() {
        $('.password-group').find('.password-box').each(function(index, input) {
            var $input = $(input);
            $input.parent().find('.password-visibility').click(function() {
                var change = "";
                if ($(this).find('i').hasClass('mdi mdi-eye-outline')) {
                    $(this).find('i').removeClass('mdi mdi-eye-outline')
                    $(this).find('i').addClass('mdi mdi-eye-off-outline')
                    change = "text";
                } else {
                    $(this).find('i').removeClass('mdi mdi-eye-off-outline')
                    $(this).find('i').addClass('mdi mdi-eye-outline')
                    change = "password";
                }
                var rep = $("<input type='" + change + "' class='form-control' style='width: 95%;'/>")
                    .attr('id', $input.attr('id'))
                    .attr('name', $input.attr('name'))
                    .attr('class', $input.attr('class'))
                    .val($input.val())
                    .insertBefore($input);
                $input.remove();
                $input = rep;
            }).insertAfter($input);
        });
    });

</script>
@endpush
