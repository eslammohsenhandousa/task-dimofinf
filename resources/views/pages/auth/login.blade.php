<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            @lang('Login') | {{ env('APP_NAME') }} - @lang('Dashboard')
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link href="{{ url('/assets/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card overflow-hidden">
                            <div class="bg-primary bg-soft">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="text-primary p-4">
                                            <h5 class="text-primary">@lang('Welcome Back') !</h5>
                                            <p>@lang('Sign in to system').</p>
                                        </div>
                                    </div>
                                    <div class="col-5 align-self-end">
                                        <img src="{{ url('/assets/images/profile-img.png') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-0">
                                <div class="auth-logo">
                                    <a href="#!" class="auth-logo-light">
                                        <div class="avatar-md profile-user-wid mb-4">
                                            <span class="avatar-title rounded-circle bg-light">
                                                <img src="{{ url('/assets/images/logo-light.svg') }}" alt="" class="rounded-circle" height="34">
                                            </span>
                                        </div>
                                    </a>

                                    <a href="#!" class="auth-logo-dark">
                                        <div class="avatar-md profile-user-wid mb-4">
                                            <span class="avatar-title rounded-circle bg-light">
                                                <img src="{{ url('/assets/images/logo.svg') }}" alt="" class="rounded-circle" height="34">
                                            </span>
                                        </div>
                                    </a>
                                </div>
                                <div class="p-2">
                                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="mb-3">
                                            <label for="email" class="form-label">
                                                @lang('Email')
                                            </label>
                                            <input
                                                type="email"
                                                class="form-control"
                                                name="email"
                                                id="email"
                                                value="{{ old('email') }}"
                                                placeholder="@lang('Enter email')">
                                        </div>
                                        @error('email')
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <i class="mdi mdi-alert-outline me-2"></i>
                                                    {{ $errors->first('email') }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                            </div>
                                        @enderror

                                        <div class="mb-3">
                                            <label class="form-label">@lang('Password')</label>
                                            <div class="input-group auth-pass-inputgroup">
                                                <input type="password" class="form-control" placeholder="@lang('Enter password')" name="password" aria-label="Password" aria-describedby="password-addon">
                                                <button class="btn btn-light " type="button" id="password-addon"><i class="mdi mdi-eye-outline"></i></button>
                                            </div>
                                        </div>
                                        @error('password')
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <i class="mdi mdi-alert-outline me-2"></i>
                                                    {{ $errors->first('password') }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                            </div>
                                        @enderror

                                        <div class="form-check">
                                            <input class="form-check-input" name="remember_me" type="checkbox" id="remember-check">
                                            <label class="form-check-label" for="remember-check">
                                                @lang('Remember me')
                                            </label>
                                        </div>

                                        <div class="mt-3 d-grid">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">@lang('Log In')</button>
                                        </div>
                                            <div class="mt-4 text-center">
                                                <h5 class="font-size-14 mb-3">@lang('Sign in with')</h5>
                
                                                <ul class="list-inline">
                                                    <li class="list-inline-item">
                                                        <a href="javascript::void()" class="social-list-item bg-primary text-white border-primary">
                                                            <i class="mdi mdi-facebook"></i>
                                                        </a>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <a href="javascript::void()" class="social-list-item bg-info text-white border-info">
                                                            <i class="mdi mdi-twitter"></i>
                                                        </a>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <a href="javascript::void()" class="social-list-item bg-danger text-white border-danger">
                                                            <i class="mdi mdi-google"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="mt-4 text-center">
                                                <a href="{{ route('password.request') }}" class="text-muted"><i class="mdi mdi-lock me-1"></i> @lang('Forgot your password?')</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <div class="mt-5 text-center">

                            <div>
                                <p>@lang("Don't have an account ?") <a href="{{ route('register') }}" class="fw-medium text-primary"> @lang('Signup now') </a> </p>
                                <p>{{ date('Y') }} © {{ env('APP_NAME') }}. - @lang('Development by eslam mohsen handousa')</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script src="{{ url('/assets/libs/jquery/jquery.min.js') }}"></script>
        <script src="{{ url('/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ url('/assets/libs/metismenu/metisMenu.min.js') }}"></script>
        <script src="{{ url('/assets/libs/simplebar/simplebar.min.js') }}"></script>
        <script src="{{ url('/assets/libs/node-waves/waves.min.js') }}"></script>
        <script src="{{ url('/assets/js/app.js') }}"></script>
    </body>
</html>
