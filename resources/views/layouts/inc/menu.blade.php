<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu"><span style="font-size: 15px">@lang('Menu')</span></li>
                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span key="t-dashboards">
                            @lang('Dashboard')
                        </span>
                    </a>
                </li>
                @if (\Auth::user()->user_type == "admin")
                    <li>
                        <a href="{{ route('users.index') }}" class="waves-effect">
                            <i class="bx bx-home-circle"></i>
                            <span key="t-dashboards">
                                @lang('Users')
                            </span>
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{ route('ads.index') }}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span key="t-dashboards">
                            @lang('Ads')
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
