<meta charset="utf-8" />
<title>{{ env('APP_NAME') }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
<meta content="Themesbrand" name="author" />
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<base href="{{ url('/') }}">
<link rel="shortcut icon" href="assets/images/favicon.ico">
<link href="assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/libs/toastr/build/toastr.min.css">
<link href="assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
<link href="assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Changa:wght@700&display=swap" rel="stylesheet">

@stack('styles')
<style>
    body{
        font-family: 'Changa', sans-serif;
    }
    .saloka_overlay{
        background: #222736;
        width: 100%;
        height: 100vh;
        position: absolute;
        top: 0px;
        z-index: 99999;
        text-align: center;
    }
    .make_pad{
        font-weight: bolder;
        padding: 7px;
    }
    .help-block{
        color: red;
    }
    @media  only screen and (max-width: 992px) {
        .hideMe {
            display: none;
        }
        .page-title-right {
            display: none;
        }
    }
</style>
