<?php

if(!function_exists('action_table_delete')) {
    function action_table_delete($route,$index = 0) {
        return '<form action="' . $route . '" method="post" id="form_'.$index.'">
        <input name="_method" type="hidden" value="delete">
        <input type="hidden" name="_token" id="csrf-token" value="' . Session::token() . '" />
        <a class="btn btn-outline-secondary btn-sm row_deleted" data-bs-toggle="modal" data-id="'.$index.'" data-bs-target="#staticBackdrop">
            <i class="fas fa-trash"></i>
        </a>
        </form>';
    }
}

if (!function_exists('display_image_by_model')) {
    function display_image_by_model($model,$key) {
        if(is_null($model)) {
            return "https://www.gravatar.com/avatar/".md5('123456');
        }
        if(is_null($model->$key)) {
            $name = explode(" ",$model->name);
            if(count($name) > 1) {
                $getName = $name[0].' '.$name[1];  
            } else {
                $getName = $model->name;
            }
            return "https://eu.ui-avatars.com/api/?uppercase=true&name=".$getName."&background=random";
        }
        return url($model->$key);
    }
}

if (!function_exists('imageUpload')) {
    function imageUpload($image, $path = null, $wh = [], $watermark = false) {
        $destinationPath = (is_null($path))? public_path('uploads'):public_path('uploads/'.$path);
        $mm = (is_null($path))?'uploads' :'uploads/'.$path;
        if(!is_dir($destinationPath)) {
            mkdir($destinationPath, 0755, true);
        }
        $imageName = time().'.'.$image->extension();  
        $image->move($destinationPath, $imageName);
        return $mm.'/'.$imageName;
    }
}


if (!function_exists('api_model_set_pagination')) {

    function api_model_set_pagination($model)
    {
        $pagnation['total'] = $model->total();
        $pagnation['lastPage'] = $model->lastPage();
        $pagnation['perPage'] = $model->perPage();
        $pagnation['currentPage'] = $model->currentPage();
        return $pagnation;
    }
}

if (!function_exists('generator_api_token')) {
    function generator_api_token() {
        $random = \Illuminate\Support\Str::random(60);
        $check = \App\Models\User::where(['api_token' => $random])->first();
        if (!is_null($check)) {
          generator_api_token();
        }
        return $random;
    }
}