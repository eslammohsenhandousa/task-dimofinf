<?php

namespace App\Http\Resources\Api\Ads;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\User\UserResource;

class AdsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'user'  => new UserResource($this->user),
            'title' => $this->title,
            'description' => $this->description,
            'image' => display_image_by_model($this,'image'),
            'created_at' => $this->created_at,
        ];
    }
}
