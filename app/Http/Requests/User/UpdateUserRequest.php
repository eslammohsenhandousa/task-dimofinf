<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;


class UpdateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $id = $this->user->id;

        if(request()->has('password') && request()->password != null)
            $rules['password'] = 'required|string|min:8';

        $rules = [
            'name'  => 'required|string|max:150',
            'email'  => 'required|email|unique:users,email,'.$id.',id',
            'avatar' => 'nullable|image|mimes:png,jpg'
        ];
        return $rules;
    }
}
