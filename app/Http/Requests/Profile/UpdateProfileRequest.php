<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [];
        if(request()->has('name')) {
            $return['name'] = 'required|min:4';
        }
        if(request()->has('email')) {
            $return['email'] = 'required|email|unique:users,email,'.\Auth::user()->id;
        }
        if(request()->has('current_password')) {
            $return['current_password']         = 'required|min:8';
            $return['new_password']             = 'required|required_with:password_confirmation|same:password_confirmation|min:8';
            $return['password_confirmation']    = 'required|min:8';
        }
        return $return;
    }
}
