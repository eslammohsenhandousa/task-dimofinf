<?php

namespace App\Http\Requests\API\Profile;

use App\Helpers\JsonFormRequest;

class UpdateRequest extends JsonFormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email'         => 'required|email|unique:users,email,'.\Auth::user()->id,
            'image'         => 'sometimes|required|mimes:png,jpg|max:8192',
        ];
    }
}
