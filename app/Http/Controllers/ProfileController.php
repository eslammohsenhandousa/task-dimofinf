<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Profile\UpdateProfileRequest;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index() {
        $breadcrumb = [
            'title' =>  __("Profile information"),
            'items' =>  [
                [
                    'title' =>  __("Profile information"),
                    'url'   =>  '#!',
                ]
            ],
        ];
        $user = \Auth::user();
        return view('pages.profile.index',[
            'breadcrumb'=>  $breadcrumb,
            'user'      =>  $user,
        ]);
    }

    public function store(UpdateProfileRequest $request) {
        $request = $request->all();
        if (request()->has('current_password')) {
            if (Hash::check(request('current_password'), \Auth::user()->password)) {
                $request['password'] = \Hash::make(request('new_password'));
            } else {
                return redirect()->route('profile.index')->with('danger', __('Your current password not match old password'));
            }
        }

        if(request()->has('avatar')) {
            $request['avatar'] = imageUpload($request['avatar'],'users');
        } else {
            if (isset($request['avatar'])){
                unset($request['avatar']);
            }
        }
        \Auth::user()->update($request);
        return redirect()->route('profile.index')->with('success',__('Your profile has been updated'));
    }
}
