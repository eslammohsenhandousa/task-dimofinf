<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Ads\CreateOrUpdateRequest;
use App\Models\Ads;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    public function index() {

        $breadcrumb = [
            'title' =>  __("Ads lists"),
            'items' =>  [
                [
                    'title' =>  __("Ads Lists"),
                    'url'   =>  '#!',
                ]
            ],
        ];
        $lists = Ads::orderBy('id', 'desc')->paginate();
        return view('pages.ads.index',[
            'breadcrumb'=>$breadcrumb,
            'lists'=>$lists,
        ]);
    }

    public function create() {
        $breadcrumb = [
            'title' =>  __("Create new ads"),
            'items' =>  [
                [
                    'title' =>  __("Ads Lists"),
                    'url'   => route('ads.index'),
                ],
                [
                    'title' =>  __("Create new ads"),
                    'url'   =>  '#!',
                ],
            ],
        ];

        return view('pages.ads.create',[
            'breadcrumb'=>$breadcrumb,
        ]);
    }

    public function store(CreateOrUpdateRequest $request) {
        $request = $request->all();
        $request = $this->uploadFiles($request , $request);
        \Auth::user()->ads()->create($request);
        return redirect()->route('ads.index')->with('success', __("This ads has been created."));
    }

    public function show(Ads $ad) {
        $breadcrumb = [
            'title' =>  __("Show ads"),
            'items' =>  [
                [
                    'title' =>  __("Ads Lists"),
                    'url'   => route('ads.index'),
                ],
                [
                    'title' =>  __("Show ads"),
                    'url'   =>  '#!',
                ],
            ],
        ];

        return view('pages.ads.show',[
            'breadcrumb'=>$breadcrumb,
            'ad'=>$ad,
        ]);
    }

    public function edit(Ads $ad) {
        if(\Auth::user()->id != $ad->user_id) {
            return redirect()->route('ads.index')->with('success', __("You can't edit this ads."));
        }
        $breadcrumb = [
            'title' =>  __("Edit ads"),
            'items' =>  [
                [
                    'title' =>  __("Ads Lists"),
                    'url'   => route('ads.index'),
                ],
                [
                    'title' =>  __("Edit ads"),
                    'url'   =>  '#!',
                ],
            ],
        ];

        return view('pages.ads.edit',[
            'breadcrumb'=>$breadcrumb,
            'ad'=>$ad,
        ]);
    }

    public function update(CreateOrUpdateRequest $request, Ads $ad) {
        if(\Auth::user()->id != $ad->user_id) {
            return redirect()->route('ads.index')->with('success', __("You can't update this ads."));
        }
        $request = $request->all();
        $request = $this->uploadFiles($request , $request);
        $ad->update($request);
        return redirect()->route('ads.index')->with('success', __("This ads has been updated."));
    }

    public function destroy(Request $request,Ads $ad) {
        if(\Auth::user()->id != $ad->user_id) {
            return redirect()->route('ads.index')->with('success', __("You can't delete this ads."));
        }
        $ad->delete();
        return redirect()->route('ads.index')->with('success', __("This ads has been deleted."));
    }

    public function uploadFiles($request)
    {
        if(request()->has('image') && !is_null(request('image'))) {
            $request['image'] = imageUpload($request['image'],'adv');
        } else {
            if (isset($request['image'])){
                unset($request['image']);
            }
        }

        return $request;
    }
}
