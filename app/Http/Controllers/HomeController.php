<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        return view('pages.home.index');
    }

    public function logout() {
        \Auth::logout();
        return redirect()->route('home');
    }
}
