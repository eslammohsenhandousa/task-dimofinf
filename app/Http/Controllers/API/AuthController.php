<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
// Request
use Illuminate\Http\Request;
use App\Http\Requests\API\Auth\LoginRequest;
use App\Http\Requests\API\Auth\RegisterRequest;
// API
use App\Support\API;
// Resources
use App\Http\Resources\Api\User\UserResource;
// Hash
use Illuminate\Support\Facades\Hash;
// Models
use App\Models\User;
class AuthController extends Controller
{
    public function login(LoginRequest $request) {
        $user = User::where([
            'email'     => $request->email,
        ])->first();
        if(!$user) {
            return (new API)->isError(__("This User not found"))->build();
        }
        if(!\Hash::check($request->password, $user->password)){
            return (new API)->isError(__("Your password is incorrect"))->build();
        }
        $user->update(['api_token' => generator_api_token()]);
        return (new API)->isOk(__("User Information"))->setData(new UserResource($user))->addAttribute('api_token',$user->api_token)->build();
    }

    public function register(RegisterRequest $request) {
        $data = $request->all();
        $data['password'] = Hash::make($request['password']);
        $data['user_type'] = "client";
        $data['api_token'] = generator_api_token();
        $user = User::create($data);
        return (new API)->isOk(__("User Information"))->setData(new UserResource($user))->addAttribute('api_token',$user->api_token)->build();
    }
}
