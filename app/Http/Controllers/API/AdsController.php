<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
// Request
use Illuminate\Http\Request;
use App\Http\Requests\API\Ads\CreateOrUpdateRequest;
// Resources
use App\Http\Resources\Api\Ads\AdsResource;
// Models
use App\Models\Ads;
// API
use App\Support\API;

class AdsController extends Controller
{
    public function index() {
        $lists = Ads::orderBy('id', 'desc')->paginate();
        return (new API)->isOk(__("Ads lists"))->setData(AdsResource::collection($lists))->addAttribute('paginate',api_model_set_pagination($lists))->build();
    }

    public function myAds() {
        $lists = \Auth::user()->ads()->orderBy('id', 'desc')->paginate();
        return (new API)->isOk(__("My ads lists"))->setData(AdsResource::collection($lists))->addAttribute('paginate',api_model_set_pagination($lists))->build();
    }

    public function show(Ads $ad) {
        return (new API)->isOk(__("ads information"))->setData(new AdsResource($ad))->build();
    }

    public function store(CreateOrUpdateRequest $request, Ads $ad) {
        $request = $request->all();
        $request = $this->uploadFiles($request , $request);
        $ad = \Auth::user()->ads()->create($request);
        return (new API)->isOk(__("ads information"))->setData(new AdsResource($ad))->build();
    }

    public function update(CreateOrUpdateRequest $request,Ads $ad) {
        if(\Auth::user()->id != $ad->user_id) {
            return (new API)->isError(__("You can't updated this ads."))->build();
        }
        $request = $request->all();
        $request = $this->uploadFiles($request , $request);
        $ad->update($request);
        return (new API)->isOk(__("this ads has been updated"))->setData(new AdsResource($ad))->build();
    }

    public function destroy(Ads $ad) {
        if(\Auth::user()->id != $ad->user_id) {
            return (new API)->isError(__("You can't delete this ads."))->build();
        }
        $ad->delete();
        return (new API)->isOk(__("this ads has been delete"))->build();
    }


    public function uploadFiles($request)
    {
        if(request()->has('image') && !is_null(request('image'))) {
            $request['image'] = imageUpload($request['image'],'adv');
        } else {
            if (isset($request['image'])){
                unset($request['image']);
            }
        }

        return $request;
    }
}
