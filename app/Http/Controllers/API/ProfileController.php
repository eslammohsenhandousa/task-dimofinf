<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
// Request
use Illuminate\Http\Request;
use App\Http\Requests\API\Profile\UpdateRequest;
// API
use App\Support\API;
// Resources
use App\Http\Resources\Api\User\UserResource;
// Hash
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index() {
        return (new API)->isOk(__("Profile Information"))->setData(new UserResource(\Auth::user()))->build();
    }
    
    public function update(UpdateRequest $request) {
        $data = $request->all();
        if(request()->has('password') && !is_null(request('password'))) {
            $data['password'] = Hash::make($request['password']);
        } else {
            unset($data['password']);
        }
        $data = $this->uploadFiles($request , $data);
        if(!empty($data)) {
            \Auth::user()->update($data);
        }
        return (new API)->isOk(__("Profile Information"))->setData(new UserResource(\Auth::user()))->build();
    }

    public function uploadFiles($request , $data)
    {
        if(request()->has('avatar') && !is_null(request('avatar'))) {
            $data['avatar'] = imageUpload($request['avatar'],'users');
        } else {
            if (isset($request['avatar'])){
                unset($request['avatar']);
            }
        }

        return $data;
    }
}
