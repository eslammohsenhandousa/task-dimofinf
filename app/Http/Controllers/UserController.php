<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index() {

        $breadcrumb = [
            'title' =>  __("Users lists"),
            'items' =>  [
                [
                    'title' =>  __("Users Lists"),
                    'url'   =>  '#!',
                ]
            ],
        ];

        return view('pages.users.index',[
            'breadcrumb'=>$breadcrumb,
            'lists'=>User::orderBy('created_at', 'desc')->paginate(),
        ]);
    }

    public function create() {
        $breadcrumb = [
            'title' =>  __("Create New User"),
            'items' =>  [
                [
                    'title' =>  __("Users Lists"),
                    'url'   => route('users.index'),
                ],
                [
                    'title' =>  __("Create New User"),
                    'url'   =>  '#!',
                ],
            ],
        ];

        return view('pages.users.create',[
            'breadcrumb'=>$breadcrumb,
        ]);
    }

    public function store(CreateUserRequest $request) {
        $data = $request->all();
        $data['password'] = Hash::make($request['password']);
        $data['email_verified_at'] = Carbon::now();
        $data = $this->uploadFiles($request , $data);
        $user = User::create($data);
        return redirect()->route('users.index')->with('success', __("This user has been created."));
    }

    public function edit(User $user) {
        $breadcrumb = [
            'title' =>  __("Edit user"),
            'items' =>  [
                [
                    'title' =>  __("Users Lists"),
                    'url'   => route('users.index'),
                ],
                [
                    'title' =>  __("Edit user"),
                    'url'   =>  '#!',
                ],
            ],
        ];

        return view('pages.users.edit',[
            'breadcrumb'=>$breadcrumb,
            'user'=>$user,
        ]);
    }

    public function update(UpdateUserRequest $request, User $user) {
        $data = $request->all();
        if(request()->has('password') && !is_null(request('password'))) {
            $data['password'] = Hash::make($request['password']);
        } else {
            unset($data['password']);
        }
        $data = $this->uploadFiles($request , $data);
        $user->update($data);
        return redirect()->route('users.index')->with('success', __("This user has been updated."));
    }

    public function destroy(Request $request,User $user) {
        $user->delete();
        return redirect()->route('users.index')->with('success', __("This user has been deleted."));
    }

    public function uploadFiles($request , $data)
    {
        if(request()->has('avatar') && !is_null(request('avatar'))) {
            $data['avatar'] = imageUpload($request['avatar'],'users');
        } else {
            if (isset($request['avatar'])){
                unset($request['avatar']);
            }
        }

        return $data;
    }
}
