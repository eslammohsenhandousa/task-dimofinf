<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    // Logout
    Route::get('/logout', [HomeController::class,'logout'])->name('logout');
    // Profile
    Route::get('/profile', [ProfileController::class,'index'])->name('profile.index');
    Route::post('/profile', [ProfileController::class,'store'])->name('profile.store');
    // Home
    Route::get('/', [HomeController::class,'index'])->name('home');

    Route::resource('users', UserController::class);
    Route::resource('ads', AdsController::class);
});