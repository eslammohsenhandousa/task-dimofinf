<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\API\AdsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::middleware(['throttle:3,1'])->group(function () {
    // Auth
    Route::post('/login', [AuthController::class,'login']);
    Route::post('/register', [AuthController::class,'register']);

    Route::middleware('auth:api')->group(function () {
        // Profile
        Route::get('/profile', [ProfileController::class,'index']);
        Route::put('/profile', [ProfileController::class,'update']);
        // Ads
        Route::get('/my-ads', [AdsController::class,'myAds']);
        Route::get('/ads', [AdsController::class,'index']);
        Route::get('/ads/{ad}', [AdsController::class,'show'])->where('ad','[0-9]+');
        Route::post('/ads', [AdsController::class,'store']);
        Route::put('/ads/{ad}/update', [AdsController::class,'update'])->where('ad','[0-9]+');
        Route::delete('/ads/{ad}/delete', [AdsController::class,'destroy'])->where('ad','[0-9]+');
    });
// });